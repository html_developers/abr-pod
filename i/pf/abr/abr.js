$(document).ready(function(){

   $(window).resize(function(){
      resizer();
   });
   resizer();

   $('.one-preview').click(function(){
      $(this).closest('.slider').find('.one-preview').removeClass("active");
      $(this).addClass('active');
      $(this).closest('.slider').css("background-image","url('"+$(this).attr("big")+"')");
   });

   $('.button-sw').click(function(){
      $('.button-sw').removeClass("active");
      $(this).addClass('active');
      $('.logoblock').css("background-image","url('"+$(this).attr("big")+"')");
   });

});


function resizer(){
   setTimeout(function(){
      var width = $('body').width();


      if (width<1024){
         $('.left-line').css("width", "20px");
         $('.right-line').css("width", (width-20-81)+"px");
         $('.padding-left').css("padding-left","40px");
         $('.right-text').css("width", (width-40-665)+"px");
      } else {
         $('.left-line').css("width", "140px");
         $('.right-line').css("width", (width-140-81)+"px");
         $('.padding-left').css("padding-left","190px");
         $('.right-text').css("width", (width-190-665)+"px");
      }

   },10);

}